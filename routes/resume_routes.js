var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');

var skill_dal = require('../model/skill_dal');
var company_dal = require('../model/company_dal');


// View All account
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getAllAccountInfo(req.query.account_id, function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {account: req.query.account_id, skill: result[0], school: result [1], company: result [2]});
        }
    });
});

// View the company for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.resume_name == null) {
        res.send('Resume must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                resume_dal.edit(resume_id, function(err, result) {
                    res.render('resume/resumeUpdate', {resume: result [0][0], company: result[1], skill: result [2], school: result [3], was_successful: true});
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err, resume){
                company_dal.getAll(function(err, company) {
                    skill_dal.getAll(function(err, skill) {
                        resume_dal.getSchool(req.query.resume_id, function(err, school) {
                        res.render('resume/resumeUpdate', { skill: skill, school: school, company: company, resume: resume[0], was_successful: true});

                        });
                    });
                });

        });
    }

});



router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

router.get('/SelectUser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'account': result});
        }
    });
});



module.exports = router;