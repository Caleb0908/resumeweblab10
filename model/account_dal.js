var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account ;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(account_id, callback) {
    var query = 'Select * from account Where account_id = ?;';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email,first_name, last_name ) VALUES (?,?,?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData , function(err, result) {

        var account_id = result.insertId;
        callback(err,result);

    });


};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET first_name = ? , last_name = ? WHERE account_id = ?';

    var queryData = [params.first_name,params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo (?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.email = function(params, callback){
    var query = 'Select email from account where account_id = ?';
    var queryData =[params.email, params.account_id];


    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};