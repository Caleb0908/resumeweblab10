var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'select r.resume_id, r.resume_name, a.* from resume r left join account a on a.account_id = r.account_id Order by first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'Select * from resume Where resume_id = ?';
    var queryData = [resume_id];
    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getSchool = function( callback) {
    var query = 'select sc.* from school sc left join resume_school rs on rs.school_id = sc.school_id where rs.resume_id = ?;';
    var queryData =[resume_id];
    connection.query(query,  function(err, result) {

        callback(err, result);
    });
};

exports.getAllAccountInfo = function(account_id,callback){
    var query = 'Call account_getAllinfo (?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err,result){
        callback(err,result);
    })
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE resume
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?,?)';

    var queryData = [ params.account_id, params.resume_name];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result.insertId;

        var query = 'Insert Into resume_skill (resume_id, skill_id) VALUES ?';
        var SkillResumeData = [];
        for (var i = 0; i < params.skill_id.length; i++) {
            SkillResumeData.push([resume_id, params.skill_id[i]]);
        }
        connection.query(query, [SkillResumeData], function (err, result) {
            // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
            var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var ResumeSchoolData = [];
            for (var i = 0; i < params.school_id.length; i++) {
                ResumeSchoolData.push([resume_id, params.school_id[i]]);
            }
            // NOTE THE EXTRA [] AROUND companyAddressData
            connection.query(query, [ResumeSchoolData], function (err, result) {
                var query = 'Insert Into resume_company (resume_id, company_id) VALUES ?';
                var CompanyResumeData = [];
                for (var i = 0; i < params.company_id.length; i++) {
                    CompanyResumeData.push([resume_id, params.company_id[i]]);
                }
                connection.query(query, [CompanyResumeData], function (err, result) {
                    callback(err, resume_id);
                });
            });
        });


    });


};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM resume WHERE resume_id = ?';
        var queryData = [resume_id];
        connection.query(query, queryData, function(err, result){
            callback(err, result);
        });

    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';

    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        companyResumeDeleteAll(params.resume_id, function(err, result){

            if(params.company_id != null) {
                //insert company_address ids
                companyResumeInsert( params.company_id, params.resume_id, function(err, result){

                    schoolResumeDeleteAll(params.resume_id, function(err, result){

                        if(params.school_id != null) {
                            //insert company_address ids
                            schoolResumeInsert(params.resume_id, params.school_id, function(err, result){

                                skillResumeDeleteAll(params.resume_id, function(err, result){

                                    if(params.skill_id != null) {
                                        //insert company_address ids
                                        SkillResumeInsert(params.resume_id, params.skill_id, function(err, result){

                                            callback(err, result);
                                        });}
                                    else {
                                        callback(err, result);
                                    }

                                });
                            });}
                        else {
                            callback(err, result);
                        }

                    });
                });}
            else {
                callback(err, result);
            }
        });
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo (?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//declare the function so it can be used locally
var SkillResumeInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (skill_id, resume_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var SkillResumeData = [];
    for(var i=0; i < SkillResumeData.length; i++) {
        SkillResumeData.push([resume_id, skillIdArray[i]]);
    }
    connection.query(query, [SkillResumeData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.SkillResumeInsert = SkillResumeInsert;

//declare the function so it can be used locally
var companyResumeInsert = function(company_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?,?)';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var CompanyResumeData = [];
    for(var i=0; i < CompanyResumeData.length; i++) {
        CompanyResumeData.push([resume_id, companyIdArray[i]]);
    }
    connection.query(query, [CompanyResumeData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyResumeInsert = companyResumeInsert;

var schoolResumeInsert = function(school_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?,?)';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var ResumeSchoolData = [];
    for(var i=0; i < ResumeSchoolData.length; i++) {
        ResumeSchoolData.push([resume_id, schoolIdArray[i]]);
    }
    connection.query(query, [ResumeSchoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolResumeInsert = schoolResumeInsert;




//declare the function so it can be used locally
var companyResumeDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyResumeDeleteAll = companyResumeDeleteAll;

var schoolResumeDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolResumeDeleteAll = schoolResumeDeleteAll;

var skillResumeDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.skillResumeDeleteAll = skillResumeDeleteAll;

